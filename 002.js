// 002
function oddEven(p1) {
	return ((p1 % 2) == 0) ? 'genap' : 'ganjil';
}

console.log('2.');
console.log('oddEven(10) : ' + oddEven(10));
console.log('oddEven(5) : ' + oddEven(5));