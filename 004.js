// 004
function palindrome(p1) {
	var regex = /[^A-Za-z0-9]/g;
	var str1 = p1.toLowerCase().replace(regex, '');
	var str2 = str1.split('').reverse().join('');
	return str1 == str2;
}

console.log('4.');
console.log('palindrome("Cigar? Toss it in a can. It is so tragic") : ' + palindrome("Cigar? Toss it in a can. It is so tragic"));
console.log('palindrome("I did, did I?") : ' + palindrome("I did, did I?"));
console.log('palindrome("Red rum, sir, is murder") : ' + palindrome("Red rum, sir, is murder"));
console.log('palindrome("Eva, can I see bees in a cave?") : ' + palindrome("Eva, can I see bees in a cave?"));
console.log('palindrome("Hello World") : ' + palindrome("Hello World"));