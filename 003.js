// 003
function max(ps) {
	var value = null;
	for (var i=0; i<ps.length; i++) {
		if (value == null) {
			value = ps[i];
		} else if (value < ps[i]) {
			value = ps[i];
		}
	}
	return value;
}

function min(ps) {
	var value = null;
	for (var i=0; i<ps.length; i++) {
		if (value == null) {
			value = ps[i];
		} else if (value > ps[i]) {
			value = ps[i];
		}
	}
	return value;
}

function average(ps) {
	var value = 0;
	for (var i=0; i<ps.length; i++) {
		value += ps[i];
	}
	return value/ps.length;
}

console.log('3.');
console.log('max([1,2,3,4,5,6,7,8,9,10]) : ' + max([1,2,3,4,5,6,7,8,9,10]));
console.log('min([2,3,4,5,6,7,8,9,10]) : ' + min([2,3,4,5,6,7,8,9,10]));
console.log('average([6,7,8,9,10]) : ' + average([6,7,8,9,10]));