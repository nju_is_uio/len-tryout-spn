// 009
function removeDuplicate(ps) {
	return [...new Set(ps)];
}

console.log('9.');
console.log('removeDuplicate : ' + removeDuplicate(["Jakarta", "Aceh", "Malang", "Medan", "Bontang", "Jogja", "Jakarta", "Bandung", "Malang", "Solo", "Palembang", "Bandung"]));