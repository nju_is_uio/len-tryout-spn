// 007
function reverse(p1) {
	var value = p1.split('').reverse().join('');
	return value;
}

console.log('7.');
console.log('reverse("semua kata-kata") : ' + reverse("semua kata-kata"));