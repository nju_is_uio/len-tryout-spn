// 005
function leapYear(p1, p2) {
	var values = [];
	for (var i=p1; i<=p2; i++) {
		if (((i%4)==0) && (((i%100)!=0) || ((i%400)==0))) {
			values.push(i);
		}
	}
	return values;
}

console.log('5.');
console.log('leapYear(1600,2020) : ' + leapYear(1600,2020));