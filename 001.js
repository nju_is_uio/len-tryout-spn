// 001
function grade(p1) {
	if (p1 >= 90) {
		return 'A';
	}
	else if (p1 >= 80) {
		return 'B';
	}
	else if (p1 >= 70) {
		return 'C';
	}
	else if (p1 >= 60) {
		return 'D';
	}
	else {
		return 'E';
	}
  return p1;
}

console.log('1.');
console.log('grade(10) : ' + grade(10));
console.log('grade(80) : ' + grade(80));
console.log('grade(90) : ' + grade(90));