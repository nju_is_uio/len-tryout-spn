// 010
function below21(ps) {
	var values = [];
	for (var i=0; i<ps.length; i++) {
		if (ps[i].age < 21) {
			values.push(ps[i]);
		}
	}
	return values;
}

console.log('10.');
console.log('below21 : ');
console.log(
	below21(
		[
			{ "id": 1, "name": "Udin", "age": 12 },
			{ "id": 2, "name": "Reane", "age": 51 },
			{ "id": 3, "name": "Budi", "age": 34 },
			{ "id": 4, "name": "Agus", "age": 16 },
			{ "id": 5, "name": "Sari", "age": 19 },
			{ "id": 6, "name": "Ririn", "age": 20 },
			{ "id": 7, "name": "Dessy", "age": 23 }
		]
	)
);